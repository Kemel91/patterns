<?php

namespace Rebirth\Builder;


class Pizza
{
    public $size = 10;

    public $cheese = false;
    public $meat = false;
    public $tomato = false;

    public function __construct(PizzaBuilder $builder)
    {
        $this -> size = $builder -> size;
        $this -> cheese = $builder->cheese;
        $this -> meat = $builder -> meat;
        $this -> tomato = $builder -> tomato;
    }

    /**
     * @return int|int
     */
    public function getSize()
    {
        return $this->size;
    }

}