<?php

use Rebirth\Builder\PizzaBuilder;

$pizza = (new PizzaBuilder(14))
            -> setCheese()
            -> setMeat()
            -> setSize(18)
            -> build();

//echo $pizza->getSize();

var_dump($pizza);
