<?php

namespace Rebirth\Builder;


class PizzaBuilder
{
    public $size;

    public $cheese = false;
    public $meat = false;
    public $tomato = false;

    public function __construct(int $size)
    {
        $this -> size = $size;
    }

    /**
     * @param mixed $size
     * @return PizzaBuilder
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return PizzaBuilder
     */
    public function setCheese()
    {
        $this->cheese = true;
        return $this;
    }

    /**
     * @return PizzaBuilder
     */
    public function setMeat()
    {
        $this->meat = true;
        return $this;
    }

    /**
     * @return PizzaBuilder
     */
    public function setTomato()
    {
        $this->tomato = true;
        return $this;
    }

    public function build() {
        return new Pizza($this);
    }

}