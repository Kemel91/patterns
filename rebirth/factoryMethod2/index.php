<?php
$backQA = new \Rebirth\factoryMethod2\BackendQA();
$backQA -> doTask();

echo nl2br(PHP_EOL);

$frontQA = new \Rebirth\factoryMethod2\FrontendQA();
$frontQA -> doTask();
