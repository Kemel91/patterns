<?php

namespace Rebirth\factoryMethod2;

interface Task
{

    public function taskInfo();

}