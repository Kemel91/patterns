<?php

namespace Rebirth\factoryMethod2;

class BackendQA extends QAEngineer
{

    function makeTask(): Task
    {
        return new BackendTask();
    }
}