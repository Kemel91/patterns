<?php

namespace Rebirth\factoryMethod2;

class BackendTask implements Task
{

    public function taskInfo()
    {
        echo 'Сделать API для сайта';
    }
}