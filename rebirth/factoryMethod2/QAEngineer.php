<?php

namespace Rebirth\factoryMethod2;

abstract class QAEngineer
{
    abstract function makeTask():Task;

    public function doTask() {
        $task = $this -> makeTask();
        $task -> taskInfo();
    }
}