<?php


namespace Rebirth\factoryMethod2;


class FrontendQA extends QAEngineer
{
    function makeTask(): Task
    {
        return new FrontendTask();
    }
}