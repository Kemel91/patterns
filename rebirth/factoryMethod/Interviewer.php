<?php

namespace Rebirth\factoryMethod;

interface Interviewer
{
    public function askQuestion();
}