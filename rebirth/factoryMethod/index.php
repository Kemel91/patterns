<?php

$devManager = new \Rebirth\factoryMethod\DeveloperManager();
$devManager -> takeInterview();
echo nl2br(PHP_EOL);
$clientManager = new \Rebirth\factoryMethod\ClientManager();
$clientManager -> takeInterview();
