<?php

namespace Rebirth\factoryMethod;

class Client implements Interviewer
{

    public function askQuestion()
    {
        echo 'Asking client question';
    }
}