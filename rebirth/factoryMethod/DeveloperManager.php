<?php

namespace Rebirth\factoryMethod;

class DeveloperManager extends Manager
{
    public function makeInterviewer(): Interviewer
    {
        return new Developer();
    }
}