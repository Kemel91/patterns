<?php

namespace Rebirth\factoryMethod;

class ClientManager extends Manager
{

    public function makeInterviewer(): Interviewer
    {
        return new Client();
    }
}