<?php

namespace Rebirth\factoryMethod;

class Developer implements Interviewer
{

    public function askQuestion()
    {
        echo 'Asking about design patterns PHP';
    }
}