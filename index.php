<?php
require_once './vendor/autoload.php';

$patterns = [
    'builder' => ['name' => 'Строитель', 'path' => 'rebirth/builder'],
    'singleton' => ['name' => 'Одиночка', 'path' => 'rebirth/singleton'],
    'factoryMethod' => ['name' => 'Фабричный метод', 'path' => 'rebirth/factoryMethod'],
    'factoryMethod2' => ['name' => 'Фабричный метод 2', 'path' => 'rebirth/factoryMethod2'],
    'templateMethod' => ['name' => 'Шаблонный метод', 'path' => 'behavior/templateMethod'],
];

$route = (isset($_GET['pattern']) && array_key_exists($_GET['pattern'],$patterns)) ? trim($_GET['pattern']) : null;
if (is_null($route)) {
    foreach ($patterns as $key => $select) {
        echo "<p><a href='/?pattern=${key}'>${select['name']}</a></p>";
    }
} else {
    try {
        $path = __DIR__ . DIRECTORY_SEPARATOR . $patterns[$route]['path'] . DIRECTORY_SEPARATOR . 'index.php';
        if (!file_exists($path)) throw new HttpException('No such pattern file');
        echo '<a href="/index.php">Back</a>';
        echo '<h2>RESULT</h2>';
        \Composer\Autoload\includeFile($path);
        echo '<h2>USE</h2><br>';
        echo highlight_file($path, true);
        $dir = opendir($patterns[$route]['path']);
        $no_read = ['index.php','.','..'];
        while ($file = readdir($dir)) {
            if (in_array($file,$no_read)) continue;
            echo "<h2>${file}</h2>";
            echo highlight_file($patterns[$route]['path']. DIRECTORY_SEPARATOR . $file, true);
        }
    } catch (HttpException $e) {
        echo $e ->getMessage();
    }
}
