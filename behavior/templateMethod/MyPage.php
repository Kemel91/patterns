<?php

namespace Behavior\templateMethod;

class MyPage extends PageRender
{

    public function head()
    {
        echo '<h1>I Am Header</h1>';
    }

    public function body()
    {
        echo '<p>I am body text</p>';
    }

    public function footer()
    {
        echo '<sup>I am footer</sup>';
    }
}