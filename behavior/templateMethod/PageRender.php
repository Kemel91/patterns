<?php

namespace Behavior\templateMethod;

abstract class PageRender
{
    final public function render() {
        $this -> head();
        $this -> body();
        $this -> footer();
    }
    abstract public function head();
    abstract public function body();
    abstract public function footer();
}